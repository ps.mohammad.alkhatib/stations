package com.progressoft.assignments;

import com.progressoft.assignments.model.Station;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StationTest {

    @Test
    public void givenValidAttributes_WhenConstructStation_StationConstructed(){
        Station station = new Station("912850","21504",
                "HILO INTERNATIONAL AIRPORT","US",
                "HI", "PHTO","+19.719","-155.053","+0011.6",
                "19730101","20170925");

        Assertions.assertEquals("912850", station.getUSAF());
        Assertions.assertEquals("21504", station.getWBAN());
        Assertions.assertEquals("HILO INTERNATIONAL AIRPORT"
                ,station.getStationName());
        Assertions.assertEquals("US",station.getCTRY());
        Assertions.assertEquals("HI", station.getST());
        Assertions.assertEquals("PHTO", station.getCALL());
        Assertions.assertEquals("+19.719",station.getLAT());
        Assertions.assertEquals("-155.053", station.getLON());
        Assertions.assertEquals("+0011.6", station.getELEV());
        Assertions.assertEquals("19730101",station.getBEGIN());
        Assertions.assertEquals("20170925", station.getEND());
    }

    @Test
    public void givenTwoStations_WhenEqualize_returnTrue(){
        Station station1 = new Station("912850","21504",
                "HILO INTERNATIONAL AIRPORT","US",
                "HI", "PHTO","+19.719","-155.053","+0011.6",
                "19730101","20170925");
        Station station2 = new Station("912850","21504",
                "HILO INTERNATIONAL AIRPORT","US",
                "HI", "PHTO","+19.719","-155.053","+0011.6",
                "19730101","20170925");
        Assertions.assertTrue(station1.equals(station2));
        Assertions.assertTrue(station2.equals(station1));
        Assertions.assertEquals(station1.hashCode(),station2.hashCode());
    }
}
