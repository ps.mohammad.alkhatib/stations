package com.progressoft.assignments;

import com.progressoft.assignments.control.Controller;
import com.progressoft.assignments.data.FileDAO;
import com.progressoft.assignments.data.FileInitializationException;
import com.progressoft.assignments.data.FileParser;
import com.progressoft.assignments.service.Searcher;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, FileInitializationException, ParserConfigurationException, SAXException {
        Searcher searcher = new Searcher(new FileDAO(new FileParser()));
        Controller application = new Controller(searcher);
        application.run();
    }

}