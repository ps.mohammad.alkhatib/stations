package com.progressoft.assignments.service;

import com.progressoft.assignments.data.DAO;
import com.progressoft.assignments.data.FileDAO;
import com.progressoft.assignments.data.FileInitializationException;
import com.progressoft.assignments.data.FileParser;
import com.progressoft.assignments.model.Station;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Searcher {

    ArrayList<Integer> columnsNumbers;

    private DAO dao;
    public Searcher (DAO dao) throws ParserConfigurationException, IOException, SAXException {
       this.dao = dao;
       ArrayList<Integer>columns = getColumnNumbers();
       verifyColumnsNum(columns);
       this.columnsNumbers= columns;
    }

    public Searcher (DAO dao, ArrayList<Integer> columnsNumbers){
        this.dao = dao;
        verifyColumnsNum(columnsNumbers);
        this.columnsNumbers = columnsNumbers;
    }
    public static Searcher getInstance(DAO dao, ArrayList<Integer> columnsNumbers){
        return  new Searcher(dao,columnsNumbers);
    }

    private void verifyColumnsNum(ArrayList<Integer> columnsNumbers) {
        for (Integer columnsNumber : columnsNumbers) {
            if (columnsNumber < 1 || columnsNumber >11)
                throw new IllegalArgumentException("invalid columns nums");
        }
    }

    private ArrayList<Integer> getColumnNumbers() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory bf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = bf.newDocumentBuilder();
        String projectPath = System.getProperty("user.dir");
        Document document = builder.parse(projectPath+"/src/main/resources/numconfig.xml");
        NodeList list = document.getElementsByTagName("column");

        ArrayList<Integer> columnNum = new ArrayList<>();
        for (int i = 0; i < list.getLength(); i++) {
            columnNum.add(Integer.parseInt(list.item(i).getTextContent()));
        }
        return columnNum;
    }

    public ArrayList<String> searchByName(String name) throws ParserConfigurationException, IOException, SAXException {
        ArrayList<Station> stationList = dao.searchByName(name);
        return records(stationList);

    }
    public ArrayList<String>searchByCountryCode(String code){
        ArrayList<Station> stationList = dao.searchByCountryCode(code);
        return records(stationList);
    }
    public ArrayList<String>searchByGeoLocation(String lat, String lon){
        ArrayList<Station> stationList = dao.searchByGeoLocation(lat,lon);
        return records(stationList);
    }
    public ArrayList<String>searchByRangeID(String start, String end){
        ArrayList<Station> stationList =dao.searchByRangeID(start,end);
        return records(stationList);
    }



    private ArrayList<String> records(ArrayList<Station> stationList) {
        ArrayList<String> records =new ArrayList<>();
        for (Station station : stationList) {
            StringBuilder sb =new StringBuilder();
            for (int i = 0; i < columnsNumbers.size(); i++) {
                    sb.append(getField(station,columnsNumbers.get(i)));
                    sb.append("||");
            }
            records.add(sb.toString());
        }
        return records;
    }

    private static String getField(Station station, int x){
        return switch (x) {
            case 1 -> station.getUSAF();
            case 2 -> station.getWBAN();
            case 3 -> station.getStationName();
            case 4 -> station.getCTRY();
            case 5 -> station.getST();
            case 6 -> station.getCALL();
            case 7 -> station.getLAT();
            case 8 -> station.getLON();
            case 9 -> station.getELEV();
            case 10 -> station.getBEGIN();
            case 11 -> station.getEND();
            default -> throw new IllegalArgumentException("invalid column number");
        };
    }

}
