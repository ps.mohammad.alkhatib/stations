package com.progressoft.assignments.data;

import com.progressoft.assignments.model.Station;

import java.util.ArrayList;

public interface DAO {
    public ArrayList<Station> searchByName(String name);
    public ArrayList<Station> searchByCountryCode(String id);
    public ArrayList<Station> searchByGeoLocation(String lat, String lon);
    public ArrayList<Station> searchByRangeID(String start, String end);
}
