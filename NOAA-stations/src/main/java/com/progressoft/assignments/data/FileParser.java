package com.progressoft.assignments.data;

import com.progressoft.assignments.model.Station;
import com.sun.source.doctree.SystemPropertyTree;

import java.io.*;
import java.util.ArrayList;
import java.util.Properties;

public class FileParser {

    public static final int START_LINE = 23;
    public static final int END_LINE = 30000;

    public ArrayList<Station> getListOfStations() throws FileInitializationException, IOException {
        ArrayList<String> list = extractRecords();
        ArrayList<Station> stations = new ArrayList<>();
        for (int i = START_LINE; i < END_LINE; i++) {
            stations.add(extractStation(list.get(i)));
        }
        return stations;
    }

    private Station extractStation(String record) {
        String USAF = extract(record, 0, 6);
        String WBAN = extract(record, 7, 12);
        String stationName = extract(record, 13, 42);
        String CITY = extract(record, 43, 45);
        String ST = extract(record, 48, 50);
        String CALL = extract(record, 51, 55);
        String LAT = extract(record, 57, 64);
        String LOT = extract(record, 65, 73);
        String ELEV = extract(record, 74, 81);
        String BEGIN = extract(record, 82, 90);
        String END = extract(record, 91, 99);
        Station station = new Station(USAF, WBAN,
                stationName, CITY, ST, CALL, LAT, LOT, ELEV, BEGIN, END);
        return station;
    }
    private  String extract(String record, int start, int end) {
        return record.substring(start, end);
    }

    private BufferedReader getBufferReader() throws FileInitializationException, IOException {
        String pathOfStationFile = getStationFilePath();
        BufferedReader br;
        try {
            FileReader fr = new FileReader(pathOfStationFile);
            br = new BufferedReader(fr);
        } catch (FileNotFoundException e) {
            throw new FileInitializationException("failed to open file");
        }
        return br;
    }

    private String getStationFilePath() throws IOException {
        Properties prop = new Properties();
        String projectPath = System.getProperty("user.dir");
        InputStream input = new FileInputStream(projectPath+"/src/main/resources/config.properties");
        prop.load(input);
        return prop.getProperty("filePath");
    }

    private ArrayList<String> extractRecords() throws IOException, FileInitializationException {
        BufferedReader br = getBufferReader();
        ArrayList<String>lines = new ArrayList<>();
        String line = br.readLine();
        do {
            lines.add(line);
            line = br.readLine();
        }
        while (line != null);
        return lines;
    }
}
