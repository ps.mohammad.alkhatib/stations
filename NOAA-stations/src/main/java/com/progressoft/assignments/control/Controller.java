package com.progressoft.assignments.control;

import com.progressoft.assignments.service.Pair;
import com.progressoft.assignments.service.Searcher;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Controller {

    private static final int MIN_OPTION = 1;
    private static final int MAX_OPTION = 5;
    private static final int END_OPTION = 5;

    private Searcher searcher;


    public Controller(Searcher searchr){
        this.searcher = searchr;
    }
    public void run() throws ParserConfigurationException, IOException, SAXException {

        while (true) {
            showMenu();
            int option = readOption();
            verifyOption(option);
            if (option == END_OPTION)
                break;
            performOption(option);
        }
    }

    private void performOption(int option) throws ParserConfigurationException, IOException, SAXException {
        if (option == 1){
            String name = readName();
            ArrayList<String> strings=searcher.searchByName(name);
            printer(strings);
        }
        if (option == 2){
            String code = readCode();
            ArrayList<String> strings = searcher.searchByCountryCode(code);
            printer(strings);
        }
        if (option == 3){
            Pair pair = readPairOFValues();
            verifyIds(pair);
            ArrayList<String> strings = searcher.searchByRangeID(pair.getFirst(), pair.getSecond());
            printer(strings);
        }
        if (option == 4){
           Pair pair= readPairOFValues();
            ArrayList<String> strings =
                    searcher.searchByGeoLocation(pair.getFirst(),pair.getSecond());
            printer(strings);
        }
    }

    private Pair readPairOFValues() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter first value:");
        String first = in.nextLine();
        System.out.println();
        System.out.print("Enter second value:");
        String second =in.nextLine();
        return new Pair(first,second);
    }

    private String readCode() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter country code:");
        return in.nextLine();
    }

    private void printer(ArrayList<String> strings) {
        for (String string : strings) {
            System.out.println(string);
        }
    }

    private String  readName() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter station name:");
        return in.nextLine();
    }

    private int readOption() {
        Scanner in = new Scanner(System.in);
        System.out.print("your option:");
        return in.nextInt();
    }

    private void showMenu(){
        System.out.println("Enter your selection: ");
        System.out.println("    1-Search for station by name");
        System.out.println("    2-Search stations by country code");
        System.out.println("    3-Search stations by stations ID range");
        System.out.println("    4-Search stations by Geographical location");
        System.out.println("    5-END");
    }
    public void verifyOption(int option){
        if (option < MIN_OPTION || option > MAX_OPTION)
            throw new IllegalArgumentException("wrong choice");
    }

    public void verifyIds(Pair pair){
        if (!isNumeric(pair.getFirst()) || !isNumeric(pair.getSecond()))
            throw new IllegalArgumentException("should be numeric values to be range");
    }

    private boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}
